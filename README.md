## Pubg-API backup
#### Dependencies
- Python 3
- Python "requests" module
#### Usage
python request_player_info.py [-n <i>playerName</i>] [-i <i>playerId</i>] [-s] [-C]

Either name or id needs to be specified. Specifying only name will perform one additional
request each time to get player ID based on name. File "playerID.json" is checked in root dir for
saved name - id pairs. Upon first using a name or id that's not in there, the pair is added to the file
and used from there on subsequent runs.

-s flag also downloads lifetime stats for player. Omit this to perform one request fewer.

-C flag disables color printouts (for terminals that don't support ANSI escape codes)

---

##### Authorization
A Pubg api auth key is required to make requests. By default, this is assumed to be in "auth-key" file in the script dir,
containing just the key.<br>
Get your API key from [https://developer.playbattlegrounds.com/](https://developer.playbattlegrounds.com/)<br>
Your friends will probably not want you to use theirs, since the default limit is 10 requests/min

---

##### Request limit
Requests are limited to 10 per minute, so by default each request waits 6 seconds before requesting again. 
Telemetry requests don't have a limit, so no delay is required.<br>

When exceeding request amount for a specific api key, a 429 status is returned,
which will not have any other side-effects. <br>

The script will currently keep re-trying indefinitely every 6 seconds until a 200 (success) is returned. Kill it with fire if you don't like that.

Getting a 401 (Not authorized) probably means your API key is invalid. This, and some other conditions exit immediately, 
so data might not get downloaded properly

---

##### Output
Output will appear in "output" dir in the script root.
- General player data in "output/<i>playerName</i>/player_data.json"
- Player lifetime stats (if enabled) in "output/<i>playerName</i>/lifetime_stats.json"
- Match data in "output/<i>playerName</i>/matches/<i>matchId</i>.json"
- Telemetry in "output/<i>playerName</i>/matches/telemetry/TELE_<i>matchId</i>.json"