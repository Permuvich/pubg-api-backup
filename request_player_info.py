import argparse
import json
import os
import errno
import sys
import requests
from time import sleep
from termcolor import cprint

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--name', help='Player name if account ID is not known (additional request)')
parser.add_argument('-i', '--id', help='Player accountId if known - recommended')
parser.add_argument('-s', action='store_true', help='Fetch player lifetime stats')
parser.add_argument('-C', action='store_true', help='Disable color printouts if terminal does not support')

args = parser.parse_args()

PLAYER_NAME = args.name
PLAYER_ID = args.id
INCLUDE_STATS = args.s
NO_COLOR = args.C


def print_color(src, color='white', attrs=None, end='\n'):
    if NO_COLOR:
        print(src, end=end)
    else:
        cprint(src, color, attrs=attrs, end=end)


def print_bold(src):
    print_color(src, 'white', attrs=['bold'])


def print_ok(src):
    print_color(src, 'green')


def print_err(src):
    print_color(src, 'red')


if not args.name and not args.id:
    print_color("Usage: \n")
    print_color("\tpython request_player_info.py [-i accountId] [-n playerName] [-s] [-C]\n", 'blue')
    print_color("\tSpecify accountId if possible, both not needed\n\t-s - include request for lifetime stats\n\t-C - disable color printouts")
    sys.exit()

# File containing only the key for authorization
KEY_FILE = "auth-key"

# Get AUTH_KEY from file
try:
    with open(KEY_FILE, 'r') as f:
        AUTH_KEY = f.read()
except FileNotFoundError:
    print_err("Could not find file " + KEY_FILE + " for PUBG API key")
    sys.exit()

# Check if file contains anything
if not AUTH_KEY:
    print_err("No API key found in file " + KEY_FILE)
    sys.exit()

# General vars

DELAY = 6
PLAYER_ID_FILE_PATH = "playerID.json"
OUTPUT_DIR = "output"
PLAYER_DATA_FILE = "player_data.json"
PLAYER_LIFETIME_STATS_FILE = "lifetime_stats.json"

PLATFORM = "steam"
BASE_URL = "https://api.pubg.com/shards/" + PLATFORM
PLAYERS_URL = BASE_URL + "/players"
MATCHES_URL = BASE_URL + "/matches"
REQUEST_HEADER = {
    "Authorization": "Bearer " + AUTH_KEY,
    "Accept": "application/vnd.api+json"
}


# Will perform a request as long as needed for a 200 response.
def performRequestWithRetry(url, headers, params=None, noRetry=False):
    while True:
        print_color("Sending request to url [" + url + "]", end='')
        if not params:
            params = {}
        r = requests.get(url, headers=headers, params=params)
        if r.status_code == 200:
            print_ok(" - SUCCESS (200)")
            return r
        elif r.status_code == 401:
            print_err(" - FAILURE (401 Unauthorized)")
            print_err("AUTH_KEY invalid! Make sure valid PUBG API key is present in " + KEY_FILE + " file")
            print()
            print_color("Exiting...")
            sys.exit()
        else:
            print_err(" - FAILURE (" + str(r.status_code) + ")")
            print_color(r.text)
            if noRetry:
                print_color("Exiting...")
                sys.exit()
            sleepDelay()
            print_color("Retrying...")


def getPlayerId(playerName):
    # Get player info by name:

    print_bold("Getting player accountId by name " + playerName)
    param = {
        "filter[playerNames]": playerName
    }

    r = performRequestWithRetry(PLAYERS_URL, REQUEST_HEADER, param, noRetry=True)
    accountId = json.loads(r.text)["data"][0]["id"]
    print_color("Got accountId=" + accountId)
    return accountId


def getPlayerData(accountId):
    print_bold("Getting general player info by accountId = " + accountId)
    r = performRequestWithRetry(PLAYERS_URL + "/" + accountId, REQUEST_HEADER, noRetry=True)
    j = json.loads(r.text)
    # printJson(j)
    path = OUTPUT_DIR + "/" + j["data"]["attributes"]["name"] + "/" + PLAYER_DATA_FILE
    writeJson(j, path)
    return j


def getPlayerLifetimeStats(accountId, playerName):
    print_bold("Getting lifetime stats for player " + accountId)
    url = PLAYERS_URL + "/" + accountId + "/seasons/lifetime"
    r = performRequestWithRetry(url, REQUEST_HEADER)
    j = json.loads(r.text)
    # printJson(j)
    path = OUTPUT_DIR + "/" + playerName + "/" + PLAYER_LIFETIME_STATS_FILE
    writeJson(j, path)


def getMatchData(matchId, playerName):
    # print_color("Getting match data for match " + matchId)
    r = performRequestWithRetry(MATCHES_URL + "/" + matchId, REQUEST_HEADER)
    j = json.loads(r.text)
    path = OUTPUT_DIR + "/" + playerName + "/matches/" + matchId + ".json"
    writeJson(j, path)

    telemetryId = j["data"]["relationships"]["assets"]["data"][0]["id"]
    for asset in j["included"]:
        if "id" in asset and asset["id"] == telemetryId:
            return matchId, asset["attributes"]["URL"]
    return matchId, None


def getMatchTelemetry(url, playerName, matchId):
    path = OUTPUT_DIR + "/" + playerName + "/matches/telemetry/TELE_" + matchId + ".json"
    header = {
        "Accept": "gzip"
    }
    r = performRequestWithRetry(url, header)
    j = json.loads(r.text)
    writeJson(j, path)


def printJson(src):
    print_color(json.dumps(src, indent=4, sort_keys=True), attrs=['dark'])


def writeJson(src, filePath):
    print_color("Writing json to file at [" + filePath + "]")
    if not os.path.exists(os.path.dirname(filePath)) and os.path.dirname(filePath) != '':
        try:
            os.makedirs(os.path.dirname(filePath))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    with open(filePath, 'w') as f:
        json.dump(src, f, indent=4, sort_keys=True)


def sleepDelay():
    print_color("Sleeping " + str(DELAY) + " seconds...")
    print()
    sleep(DELAY)


# MAIN:

# Prep:
# Get playerName - playerID json from file (if exists)
playerIDJson = json.loads("{}")
if os.path.exists(PLAYER_ID_FILE_PATH):
    try:
        playerIDJson = json.loads(open(PLAYER_ID_FILE_PATH, 'r').read())
        print_color("Got playerIDs from file:")
        print(playerIDJson)
    except json.decoder.JSONDecodeError as e:
        print_err("Could not parse json from id file: " + e.msg)
print()

# Get id if not given as argument
if not args.id:
    if playerIDJson and PLAYER_NAME in playerIDJson:
        # Get player id from file
        PLAYER_ID = playerIDJson[PLAYER_NAME]
    else:
        # Get player id via request to api
        PLAYER_ID = getPlayerId(PLAYER_NAME)
        sleepDelay()

# Get player data based on player id
playerJson = getPlayerData(PLAYER_ID)
if not playerJson:
    print_err("Could not get player data! Exiting...")
    sys.exit()

# Set name if not exists
if not args.name:
    PLAYER_NAME = playerJson["data"]["attributes"]["name"]

# Update name-id file if not exists
if PLAYER_NAME and PLAYER_ID and PLAYER_NAME not in playerIDJson:
    playerIDJson[PLAYER_NAME] = PLAYER_ID
    print_color("Adding {" + PLAYER_NAME + ": " + PLAYER_ID + "} to playerId file at [" + PLAYER_ID_FILE_PATH + "]")
    print()
    writeJson(playerIDJson, PLAYER_ID_FILE_PATH)

# Get match data
matches = playerJson["data"]["relationships"]["matches"]["data"]
print_color("Player has " + str(len(matches)) + " recorded matches")
print()
matchesToGet = []
for match in matches:
    if not os.path.exists(OUTPUT_DIR + "/" + PLAYER_NAME + "/matches/" + match["id"] + ".json"):
        matchId = match["id"]
        matchesToGet.append(matchId)

if len(matchesToGet) == 0:
    print_bold("All matches already downloaded")
    # sys.exit()
else:
    print_bold("Getting data for " + str(len(matchesToGet)) + " matches: ")
    print(matchesToGet)
print()

telemetryUrls = []  # will contain pairs of (matchId, telemetryURL)
totalMatches = len(matchesToGet)
for i, matchId in enumerate(matchesToGet):
    try:
        print_color("(" + str(i + 1) + "/" + str(totalMatches) + ") ", 'yellow', attrs=['bold'], end='')
        print_color("Getting match data for match " + matchId)
        telemetryUrl = getMatchData(matchId, PLAYER_NAME)
        telemetryUrls.append(telemetryUrl)
        sleepDelay()
    except json.decoder.JSONDecodeError as e:
        print_err("Failed to decode match data for id " + matchId + ", skipping")

# Get match telemetry:
totalTelemetry = len(telemetryUrls)
if totalTelemetry > 0:
    print_bold("Getting telemetry for " + str(totalTelemetry) + " downloaded matches")

for i, item in enumerate(telemetryUrls):
    matchId = item[0]
    telemetryUrl = item[1]
    if telemetryUrl:
        print_color("(" + str(i + 1) + "/" + str(totalTelemetry) + ") ", 'yellow', attrs=['bold'], end='')
        print_color("Getting telemetry for match " + matchId)
        getMatchTelemetry(telemetryUrl, PLAYER_NAME, matchId)
        print()
    else:
        print_err("No valid telemetry url found for match " + item[0])

# Get player stats
if INCLUDE_STATS:
    getPlayerLifetimeStats(PLAYER_ID, PLAYER_NAME)
    print()

print_ok("Finished")
